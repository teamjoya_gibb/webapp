﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace M121Webapp.Config
{
    public class Board
    {
        public int Id { get; set; }
        public string Name { get; set; }
        [JsonIgnore]
        public string Token { get; set; }
        public float LightSensor { get; set; }
        public float TempSensor { get; set; }
        public float MoistureSensor { get; set; }
        public float WaterLevelSensor { get; set; }
    }
}
