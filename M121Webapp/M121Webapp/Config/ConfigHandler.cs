﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace M121Webapp.Config
{
    public static class ConfigHandler
    {
        private static ConfigModel _model { get; set; } = new ConfigModel();
        public static List<Board> Boards
        {
            get
            {
                if (_model.Boards != null)
                    return _model.Boards;
                else
                    throw new Exception("Call first ConfigHanlder.Config(); !");
            }
        }

       public static void Config()
        {
            _model.Boards.Add(new Board()
            {
                Id = 1,
                Token = "gibbiX12345",
                Name = "My little Pony 1"
            });
        }
    }
}
