﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace M121Webapp.Config
{
    public class ConfigModel
    {
        internal List<Board> Boards { get; set; } = new List<Board>();
    }
}
