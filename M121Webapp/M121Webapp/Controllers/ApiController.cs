﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using M121Webapp.Data;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Distributed;
using Microsoft.Extensions.Logging;
using M121Webapp.Models;
using M121Webapp.Config;

namespace M121Webapp.Controllers
{
    [ArduinoAuth]
    [Produces("application/json")]
    [Route("api/")]
    public class ApiController : Controller
    {
        private DataHandler _data;
        private ILogger _logger;
        public ApiController(IDistributedCache dc, ILoggerFactory lf)
        {
            _logger = lf.CreateLogger<ApiController>();
            _data = new DataHandler(dc);
        }
        [HttpGet("{Id}")]
        public async Task<IActionResult> GetInformations(int Id)
        {
            if (Id == default(int))
                return BadRequest();
            return Ok(await _data.Get<Board>(Id));
        }
        [HttpPut("{Id}")]
        public async Task<IActionResult> SetInformations(int Id, [FromBody] Board Model)
        {
            if (!ModelState.IsValid)
                return BadRequest();

            if (Id == default(int))
                return BadRequest();

            Model.Token = ConfigHandler.Boards.First(q => q.Id == Id).Token;
            Model.Id = ConfigHandler.Boards.First(q => q.Id == Id).Id;
            Model.Name = ConfigHandler.Boards.First(q => q.Id == Id).Name;

            await _data.Set(Id, Model);
            return Accepted();
        }
    }
}