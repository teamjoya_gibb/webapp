﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace M121Webapp.Controllers
{
    public class ArduinoAuthAttribute : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            // Get APIKEY from header
            string HttpapiKey = filterContext.HttpContext.Request.Headers["ARDUINO_TOKEN"];

            // Check API-Key
            if (!String.IsNullOrEmpty(HttpapiKey))
            {
                if (!Config.ConfigHandler.Boards.Any(q => q.Token == HttpapiKey))
                    filterContext.Result = new UnauthorizedResult();
            }
            else
            {
                filterContext.Result = new UnauthorizedResult();
            }
        }
    }
}
