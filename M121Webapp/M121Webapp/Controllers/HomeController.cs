﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using M121Webapp.Config;
using M121Webapp.Data;
using M121Webapp.Models;
using M121Webapp.Models.ViewModel;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Distributed;
using Microsoft.Extensions.Logging;

namespace M121Webapp.Controllers
{
    public class HomeController : Controller
    {
        private DataHandler _data;
        private ILogger _logger;
        public HomeController(IDistributedCache dc, ILoggerFactory lf)
        {
            _logger = lf.CreateLogger<ApiController>();
            _data = new DataHandler(dc);
        }
        public IActionResult Index()
        {
            return View();
        }

        public async Task<IActionResult> RenderData()
        {
            var Model = new RenderDataViewModel();
            Model.DataSet = new List<Board>();
            foreach (var b in ConfigHandler.Boards)
            {
                var bFromData = await _data.Get<Board>(b.Id);
                if (bFromData != null)
                    Model.DataSet.Add(bFromData);
            }
            return View(Model);
        }

        public IActionResult About()
        {
            return View();
        }
    }
}