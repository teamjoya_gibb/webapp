﻿using Microsoft.Extensions.Caching.Distributed;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace M121Webapp.Data
{
    public class DataHandler
    {
        private IDistributedCache _cache;

        public DataHandler(IDistributedCache cache)
        {
            _cache = cache;
        }

        public async Task Set<T>(int Id, T value)
        {
            await _cache.SetStringAsync(Id.ToString(), JsonConvert.SerializeObject(value));
        }

        public async Task<T> Get<T>(int Id)
        {
            var value = await _cache.GetStringAsync(Id.ToString());
            return value == null ? default(T) : JsonConvert.DeserializeObject<T>(value);
        }
    }
}
