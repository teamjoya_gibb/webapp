﻿using M121Webapp.Config;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace M121Webapp.Models.ViewModel
{
    public class RenderDataViewModel
    {
        public List<Board> DataSet { get; set; }
    }
}
