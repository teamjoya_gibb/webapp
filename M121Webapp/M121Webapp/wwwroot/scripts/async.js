﻿$(".content__container").each(function () {
    var url = $(this).data("url");
    if (url && url.length > 0) {
        $(this).load(url);
    }
});